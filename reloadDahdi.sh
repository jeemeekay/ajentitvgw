#!/bin/sh

#Script to restart the dahdi drivers

service asterisk stop

service dahdi stop

service dahdi start

dahdi_cfg -vvvvv

service asterisk start