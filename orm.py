from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

engine = create_engine('mysql://vssuser:amp109@localhost/asteriskrealtime')
#engine.echo = True

print ('located base here')
Base = declarative_base()
Base.metadata.create_all(engine) 
Session = sessionmaker(bind=engine)


class SipRegistration(Base):
    __tablename__ = 'sipregistrations'
    id = Column(Integer, primary_key=True)
    username = Column(String(40))
    password = Column(String(40))
    host = Column(String(40))
    port = Column(String(5))
    active = Column(Boolean)
    
    def __init__(self,session):
        self.username = 'username'
        self.port = '5060'
        self.password = 'password'
        self.host = 'host'
        self.active = False
        session.add(self) 
    def __repr__(self):
        return "<SipRegistration('%s','%s')>" % (self.username, self.port)
try:
    SipRegistration.__table__.create(engine)
except:
    print 'Table SipRegistration already exists'

class DahdiChannel(Base):
    __tablename__ = 'dahdichannels'
    id = Column(Integer, primary_key=True)
    name = Column(String(10), unique=True)
    dahdi_name = Column(String(10))
    did = Column(String(40))
    active = Column(Boolean)
    
    def __init__(self, name, type):
        self.name = name
        self.dahdi_name = 'DAHDI/'+ self.id

    def __repr__(self):
        return "<DahdiChannel('%s','%s')>" % (self.id, self.dahdi_name)

try:
    DahdiChannel.__table__.create(engine)
except:
    print 'Table DahdiChannel already exists'

class DahdiSpan(Base):
    __tablename__ = 'dahdispans'
    id = Column(Integer, primary_key=True)
    name = Column(String(10), unique=True)
    timing = Column(Integer)
    lbo = Column(Integer)
    framing = Column(String(5))
    active = Column(Boolean)
    crc = Column(Boolean)
    coding = Column(String(5))
    range = Column(String(15))
    signalling = Column(String(15))
    winktime = Column(Integer)
    
    
    def __init__(self, name):
        self.name = name

try:
    DahdiSpan.__table__.create(engine)
except:
    print ('Table DahdiSpan already exists')

        
class DahdiRoute(Base):
    __tablename__ = 'dadhiroutes'
    id = Column(Integer, primary_key=True)
    name = Column(String(40), unique=True)
    dahdi_id = Column(Integer, ForeignKey('dahdichannels.id'))
    trunkstring = Column(String(40))
    destnum = Column(String(40))
    
    def __init__(self, session):
        session.add(self) 
        
try:
    DahdiRoute.__table__.create(engine)
except:
    print 'Table DahdiRoute already exists'
         
class SipUser(Base):
    __tablename__ = 'sipfriends'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    ipaddr = Column(String)
    port = Column(String)
    regseconds = Column(Integer)
    defaultuser = Column(String)
    fullcontact = Column(String)
    regserver = Column(String)
    useragent = Column(String)
    lastms = Column(Integer)
    host = Column(String)
	#composite_key(ipaddr, host)
    type = Column(String)
    context = Column(String)
    permit = Column(String)
    deny = Column(String)
    secret = Column(String)
    md5secret = Column(String)
    remotesecret = Column(String)
    transport = Column(String)
    dtmfmode = Column(String)
    directmedia = Column(String)
    nat = Column(String)
    callgroup = Column(String)
    pickupgroup = Column(String)
    language = Column(String)
    allow = Column(String)
    disallow = Column(String)
    insecure = Column(String)
    trustrpid = Column(String)
    progressinband = Column(String)
    promiscredir = Column(String)
    useclientcode = Column(String)
    accountcode = Column(String)
    setvar = Column(String)
    callerid = Column(String)
    amaflags = Column(String)
    callcounter = Column(String)
    busylevel = Column(Integer)
    allowoverlap = Column(String)
    allowsubscribe = Column(String)
    videosupport = Column(String)
    maxcallbitrate = Column(Integer)
    rfc2833compensate = Column(String)
    mailbox = Column(String)
	#session-timers = Column(String, 11)
	#session-expires = Column(Integer)
	#session-minse = Column(Integer)
	#sesssion-refresher = Column(String)
    t38pt_usertpsource = Column(String)
    regexten = Column(String)
    fromdomain = Column(String)
    fromuser = Column(String)
    qualify = Column(String)
    defaultip = Column(String)
    rtptimeout = Column(Integer)
    rtpholdtimeout = Column(Integer)
    sendrpid = Column(String)
    outboundproxy = Column(String)
    callbackextension = Column(String)
    registertrying = Column(String) 
    timert1 = Column(Integer)
    timerb = Column(Integer)
    qualifyfreq = Column(Integer)
    constantssrc = Column(String)
    contactdeny = Column(String)
    contactpermit = Column(String)
    usereqphone = Column(String)
    textsupport = Column(String)
    faxdetect = Column(String)
    buggymwi = Column(String)
    auth = Column(String)
    fullname = Column(String)
    trunkname = Column(String)
    cid_number = Column(String)
    callingpres = Column(String)
    mohsuggest = Column(String)
    parkinglot = Column(String)
    hasvoicemail = Column(String)
    subscribemwi = Column(String)
    vmexten = Column(String)
    autoframing = Column(String)
    rtpkeepalive = Column(Integer)
    #call-limit =  Column(Integer)
    g726nonstandard = Column(String)
    ignoresdpversion = Column(String)
    allowtransfer = Column(String)
    dynamic = Column(String)
    email = Column(String) 
    fullname = Column(String)
    trunk = Column(String)
    pin = Column(String)
    
    def __init__(self, name, type,session):
        self.name = name
        self.type = type
        session.add(self)
        self.host = 'dynamic' 

    def __repr__(self):
        return "<SipUser('%s','%s')>" % (self.name, self.type)