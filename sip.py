import json
import string
import random
from ajenti.api import *
from ajenti.plugins.main.api import SectionPlugin
from ajenti.ui import on
from ajenti.ui.binder import Binder
import globals
from orm import *
from ami import *




"""
while globals.ami_core.is_alive():
    #In a larger application, you'd probably do something useful in another non-daemon
    #thread or maybe run a parallel FastAGI server. The pystrix implementation has the AMI
    #threads run daemonically, however, so a block like this in the main thread is necessary
    time.sleep(1)

globals.ami_core.kill()
"""   

@plugin
class SIPPlugin (SectionPlugin):

    
    def init(self):
        self.title = 'SIP'  # those are not class attributes and can be only set in or after init()
        self.icon = 'sitemap'
        self.category = 'TraderVoiceGateway'
       
        
        #self.config = asterisk.config.Config('/etc/asterisk/sip_additional.conf')
        #self.categories = self.config.categories
       
        query1 = globals.session.query(SipUser).filter(SipUser.type.match('friend')).order_by(SipUser.id)
        query2 = globals.session.query(SipUser).filter(SipUser.type.match('peer')).order_by(SipUser.id)
        query3 = globals.session.query(SipRegistration).order_by(SipRegistration.id)
        self.sipusers = []
        self.siptrunks = []
        self.sipregs = []
        for u in query1.all():
            self.sipusers.append(u)
        for instance in query2.all():
            self.siptrunks.append(instance) 
        for instance in query3.all():
            self.sipregs.append(instance)
            
        """
        UI Inflater searches for the named XML layout and inflates it into
        an UIElement object tree
        """
        self.append(self.ui.inflate('tvgw:sip'))

        self.binder = Binder(self,self)
        self.binder_trunks = Binder(self.siptrunks,self.find('siptrunks'))
        self.binder_regs = Binder(self.sipregs,self.find('sipregs'))
        
        def remove_sipuser(sipuser,c):
            c.remove(sipuser)
            try:
                globals.session.delete(sipuser)
            except: 
                self.context.notify('error', 'Sipuser not persisted')    
            self.binder.reset().autodiscover().populate()
            #self.globals.session.commit()
        def remove_siptrunk(siptrunk,c):
            c.remove(siptrunk)
            try:
                globals.session.delete(siptrunk)
            except: 
                self.context.notify('error', 'Siptrunk not persisted')    
            self.binder_trunks.reset().autodiscover().populate()
            #self.globals.session.commit() 
        def remove_sipreg(sipreg,c):
            c.remove(sipreg)
            try:
                globals.session.delete(sipreg)
            except: 
                self.context.notify('error', 'Sipreg not persisted')    
            self.binder_regs.reset().autodiscover().populate()
            #self.globals.session.commit()
        
        def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
            return ''.join(random.choice(chars) for x in range(size))
            
        self.find('sip-users-list').delete_item = remove_sipuser
        self.find('sip-trunks-list').delete_item = remove_siptrunk
        self.find('sip-regs-list').delete_item = remove_sipreg
        self.find('sip-regs-list').new_item = lambda c: SipRegistration(globals.session)
        self.find('sip-trunks-list').new_item = lambda c: SipUser(id_generator(),'peer',globals.session)
        self.find('sip-users-list').new_item = lambda c: SipUser(id_generator(),'friend',globals.session)
        self.refresh()
        
    
    def refresh(self):
        self.binder.update()
        self.binder.autodiscover().populate()
        self.binder.reset().autodiscover().populate()
        self.binder_trunks.update()
        self.binder_trunks.autodiscover().populate()
        self.binder_trunks.reset().autodiscover().populate()          
        self.binder_regs.update()
        self.binder_regs.autodiscover().populate()
        self.binder_regs.reset().autodiscover().populate()        
       
    
    
    
    @on('save', 'click')
    def save(self):
        try:
            self.binder.update()
            self.binder_trunks.update()
        except:
            print ('binding updated failed..now return')
            return
        globals.session.commit()
        self.binder.reset().autodiscover().populate()
        self.binder_trunks.reset().autodiscover().update()
        self.context.notify('info', 'Sip Settings Saved')
        self.loadRegistrations()
    
    	    
   
    
    
    def loadRegistrations(self):
        query3 = globals.session.query(SipRegistration).order_by(SipRegistration.id)
        sipregs = []
        for sipreg in query3.all():
            regLine = 'register => '+sipreg.username+':'+sipreg.password+'@'+sipreg.host + ':'+sipreg.port+'\n'
            sipregs.append(regLine)
        fo = open('/etc/asterisk/sip_registrations_custom.conf','w')
        fo.writelines(sipregs)
        fo.close()
        globals.ami_core._manager.send_action(pystrix.ami.core.Command('sip reload'))
        """
        manager = asterisk.manager.Manager()
        try:
            try:
                manager.connect('localhost')
                manager.login('manager', 'mysecret')
                manager.command('sip reload')
            except asterisk.manager.ManagerSocketException, (errno, reason):
                self.context.notify('error', 'Error connecting to the manager: ' +reason)
                
            except asterisk.manager.ManagerAuthException, reason:
                self.context.notify('error', "Error logging in to the manager: " + reason)
               
            except asterisk.manager.ManagerException, reason:
                self.context.notify('error', 'Error: '+ reason)
            except: 
                self.context.notify('error','Unknown Manager Error')   
        finally: 
            manager.close()               
        """
         
                      
    
       
        
       