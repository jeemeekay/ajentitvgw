import json
from ajenti.api import *
from ajenti.plugins.main.api import SectionPlugin
from ajenti.ui import on
from ajenti.ui.binder import Binder
import globals
from orm import *
from ami import *
from sqlalchemy.exc import IntegrityError

@plugin
class RoutingPlugin (SectionPlugin):
    def init(self):
        self.title = 'Routing'  # those are not class attributes and can be only set in or after init()
        self.icon = 'sitemap'
        self.category = 'TraderVoiceGateway'

        self.append(self.ui.inflate('tvgw:dahdirouting'))
        query1 = globals.session.query(DahdiRoute).order_by(DahdiRoute.id)
        self.dahdiroutes = []
        for u in query1.all():
            self.dahdiroutes.append(u)
        self.binder = Binder(self,self)
        
        def remove_dahdiroute(dahdiroute,c):
            c.remove(dahdiroute)
            try:
                globals.session.delete(dahdiroute)
            except: 
                self.context.notify('error', 'Dahdiuser not persisted')    
            
            
        
        self.find('dahdiroutes').new_item = lambda c: DahdiRoute(globals.session)
        self.find('dahdiroutes').delete_item = remove_dahdiroute
        self.refresh()
    
    
    def on_page_load(self):
        print ('in page load')
        self.binder.reset()
        query1 = globals.session.query(DahdiChannel).filter(DahdiChannel.active == True).order_by(DahdiChannel.id)
        query2 = globals.session.query(SipUser).filter(SipUser.type.match('peer')).order_by(SipUser.id)
        
        dahdichannel_dropdown = self.find('channel-select')
        dahdichannel_dropdown.values = dhvalues = [x.id for x in query1.all()]
        dahdichannel_dropdown.labels = dhlabels = [x.dahdi_name for x in query1.all()]
        
        trunk_dropdown = self.find('trunk-select')
        
        tsvalues = []
        tslabels = []
        
        for u in query2.all():
            val = 'SIP/'+u.name
            tsvalues.append(val.replace(' ', ''))
            tslabels.append(u.name)
        
        for u in query1.all():
            tsvalues.append(u.name)
            tslabels.append(u.dahdi_name)
        trunk_dropdown.values = tsvalues
        trunk_dropdown.labels = tslabels
        
        
        
        self.binder.autodiscover().populate()
        
        
        

    
        
    def refresh(self):
        self.binder.update()
        self.binder.autodiscover().populate()
        self.binder.reset().autodiscover().populate()
        
       
    
    def updateDialplan(self):
        query1 = globals.session.query(DahdiRoute).order_by(DahdiRoute.id)
        extenconfig = '[casgw]\n\n'

        for u in query1.all():
            if u.destnum is None:
                print ('u.destnum is null')
                extenconfig = extenconfig + 'exten => '+ str(u.dahdi_id) +',1,Dial('+u.trunkstring+')\n'
            else:
                print ('u.destnum is not null')
                extenconfig = extenconfig + 'exten => '+ str(u.dahdi_id) +',1,Dial('+u.trunkstring+'/'+u.destnum+')\n'

        fo = open('/etc/asterisk/extensions_casgw.conf','w+')
        fo.write(extenconfig)
        fo.close()
        globals.ami_core._manager.send_action(pystrix.ami.core.Command('dialplan reload'))

    
    @on('protocol-select', 'change')
    def protoChange(self):
        print ('protocol change selected')  
        
    
    @on('save', 'click')
    def save(self):
        try:
            self.binder.update()
        except:
            'print bind update failed for dahdirouting'
            return
        try:
            globals.session.commit()
        except IntegrityError:
            self.context.notify('error', 'Integrity Error with Mysql Database...')
        self.binder.reset().autodiscover().populate()
        self.context.notify('info', 'Dahdi Routes Saved')
        self.updateDialplan()
    
    