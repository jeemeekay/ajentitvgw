import time
import socket

from gevent import monkey 

reload(socket)
reload(time)

import pystrix
from datetime import datetime

from apscheduler.scheduler import Scheduler


#Just a few constants for logging in. Putting them directly into code is usually a bad idea.
_HOST = 'localhost'
_USERNAME = 'manager'
_PASSWORD = 'mysecret'

class AMICore(object):
    _manager = None
    
    _kill_flag = False
    
    
    def __init__(self):
        self._manager = pystrix.ami.Manager()
        self._register_callbacks()
        sched = Scheduler()
        def do_Connect():
            try:
                print ('now in connect for ami')
                self._manager.connect(_HOST)
            
                #challenge_response = self._manager.send_action(pystrix.ami.core.Challenge())
            
                #if challenge_response and challenge_response.success:
                action = pystrix.ami.core.Login(_USERNAME,_PASSWORD)
                self._manager.send_action(action)
            
            except pystrix.ami.ManagerSocketError as e:
                self._kill_flag = True
                raise ConnectionError("Unable to connect to Asterisk server: %(error)s" % {
                'error': str(e),
                })
            except pystrix.ami.core.ManagerAuthError as reason:
                self._kill_flag = True
                raise ConnectionError("Unable to authenticate to Asterisk server: %(reason)s" % {
                'reason': reason,
               })
            except pystrix.ami.ManagerError as reason:
                self._kill_flag = True
                raise ConnectionError("An unexpected Asterisk error occurred: %(reason)s" % {
                 'reason': reason,
              })
        do_Connect()
        
        self._manager.monitor_connection()
        def job_function():
            print('in scheduled manager job...connected status is '+ str(self._manager.is_connected()))
            
            if self._manager.is_connected() == False:
                reload(socket)
                do_Connect()
                self._manager.monitor_connection()
        
        sched.start()
        sched.add_interval_job(job_function, seconds=60)    
    
    def _register_callbacks(self): 
        
        self._manager.register_callback('FullyBooted', self._handle_string_event)
        self._manager.register_callback(pystrix.ami.core_events.FullyBooted, self._handle_class_event)
        self._manager.register_callback('', self._handle_event)
        self._manager.register_callback(None, self._handle_event)
        self._manager.register_callback('Shutdown', self._handle_shutdown)
        self._manager.register_callback('FullyBooted',self._handle_booted)
    
    def _handle_shutdown(self, event, manager):
        self._kill_flag = True
        print('handle shutdown: ')
        #self._manager.disconnect()
    
    def _handle_booted(self,event,manager):
        print ('fullybooted events')
        monkey.patch_socket()
        #monkey.patch_time()
        

    def _handle_event(self, event, manager):
        print "Recieved event: %s" % event.name

    def _handle_string_event(self, event, manager):
        print "Recieved string event: %s" % event.name

    def _handle_class_event(self, event, manager):
        print "Recieved class event: %s" % event.name

    def is_alive(self):
        return not self._kill_flag

    def kill(self):
        self._manager.close()
        
class Error(Exception):
    """
    The base class from which all exceptions native to this module inherit.
    """

class ConnectionError(Error):
    """
    Indicates that a problem occurred while connecting to the Asterisk server
    or that the connection was severed unexpectedly.
    """


        
                