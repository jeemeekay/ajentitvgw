from ajenti.api import plugin
from ajenti.plugins.main.api import SectionPlugin
from ajenti.ui import on
import subprocess
from reconfigure.configs.base import Reconfig
from reconfigure.parsers import *
from reconfigure.builders import BoundBuilder
from reconfigure.items.bound import BoundData
from ajenti.ui.binder import Binder
from reconfigure.nodes import Node, PropertyNode
import globals
from orm import *



class PCMData (BoundData):
    fields = [
        'active', 'alarms', 'description', 'manufacturer', 'devicetype', 'location',
        'basechan', 'totchans', 'irq', 'irq', 'type','syncsrc','lbo','coding_opts','framing_opts','coding','framing',
        'port', 'signalling' ,'winktime','crcenabled','enabled','spandata'
    ]
    

    
    

    
    
    
for i in range(0, len(PCMData.fields)):
    PCMData.bind_property(PCMData.fields[i], PCMData.fields[i], default_remove=[None, ''])
PCMData.bind_name('name')
PCMData.bind_property('name','dahdi_name',default='')


class DahdiScanData (BoundData):
    pass

class SpanData (BoundData):
    def template(self):
        return Node('line', children=[
            Node('token', children=[PropertyNode('value', 'span')]),
            Node('token', children=[PropertyNode('value', 'value')]),
        ])
SpanData.bind_property('value', 'span', path=lambda x: x.children[0])
SpanData.bind_property('value', 'value', path=lambda x: x.children[1])
    
class LineData (BoundData):
    bits = ' '
    channel = ' '
    didnum = ' '
    def template(self):
        return Node('line', children=[
            Node('token', children=[PropertyNode('value', 'protocol')]),
            Node('token', children=[PropertyNode('value', 'value')]),
        ])

LineData.bind_property('value', 'protocol', path=lambda x: x.children[0])
LineData.bind_property('value', 'value', path=lambda x: x.children[1])    


DahdiScanData.bind_collection('channels', selector=lambda x: x.name != 'global', item_class=PCMData)

class SystemData (BoundData):
    pass

SystemData.bind_collection('spans', selector=lambda x: x.children[0]['value'].value == 'span', item_class=SpanData)
SystemData.bind_collection('lines', selector=lambda x: x.children[0]['value'].value != 'span' and x.children[0]['value'].value != 'defaultzone' and x.children[0]['value'].value != 'loadzone' and x.children[0]['value'].value != 'dynamic', item_class=LineData)



class DahdiScanConfig (Reconfig):
    def __init__(self, **kwargs):
        p = subprocess.Popen(['dahdi_scan',''], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = p.communicate()
        k = {
            'parser': IniFileParser(),
            'builder': BoundBuilder(DahdiScanData),
            'content': output,
        }
        k.update(kwargs)
        Reconfig.__init__(self, **k)        
    
class DahdiSystemConfig (Reconfig):
    def __init__(self, **kwargs):
       
        k = {
            'parser': SSVParser(separator='='),
            'builder': BoundBuilder(SystemData),
            
        }
        k.update(kwargs)
        Reconfig.__init__(self, **k)        
        
    
        

@plugin
class DahdiPlugin (SectionPlugin):
    def init(self):
        self.title = 'PCM'  # those are not class attributes and can be only set in or after init()
        self.icon = 'sitemap'
        self.category = 'TraderVoiceGateway'

        self.append(self.ui.inflate('tvgw:dahdi'))
        
        self.binder = Binder(None, self.find('config'))
        self.binder_lines = Binder(None, self.find('lines'))
        self.find('lines').new_item = lambda c: LineData()
        
        self.config = DahdiScanConfig()
        self.dahdiconfig = DahdiSystemConfig(path='/etc/dahdi/system.conf')
        
        query1 = globals.session.query(DahdiChannel).order_by(DahdiChannel.id)
        self.dahdichannels = []
       
        print ('setting active to false for channels')
        for u in query1.all():
            if u.active != False:
                u.active = False
            self.dahdichannels.append(u)
        
        globals.session.commit()
        self.refresh()

               
    def on_page_load(self):
        self.refresh()

    def refresh(self):
        self.config = DahdiScanConfig()
        self.config.load()
        self.dahdiconfig.load();
        
        
        userconfig = ''
        
        for chan in self.config.tree.channels:
            for span in self.dahdiconfig.tree.spans:
                if chan.name == span.value.split(',')[0]:
                     chan.spandata = span
                
        print ('span length: ' +str(len(self.dahdiconfig.tree.spans)))
        chanlen = 0
        if len(self.dahdiconfig.tree.spans) == len(self.config.tree.channels):
            chanlen = len(self.dahdiconfig.tree.spans)
            print ('chanlen is '+ str(chanlen))
        else:
            chanlen = len(self.config.tree.channels)
            print ('chanlen is '+ str(chanlen))
        for i in range(0, chanlen):
             
             try:
                 spanconfig = self.dahdiconfig.tree.spans[i].value.split(',')
                 if len(spanconfig) == 5:
                     self.config.tree.channels[i].crcenabled = False
                 else:
                     self.config.tree.channels[i].crcenabled = True         
             
                 self.config.tree.channels[i].syncsrc = spanconfig[1]
                 self.config.tree.channels[i].lbo = spanconfig[2]
                 self.config.tree.channels[i].coding = spanconfig[4]
                 self.config.tree.channels[i].framing = spanconfig[3]
                 self.config.tree.channels[i].signalling = globals.session.query(DahdiSpan).get(self.config.tree.channels[i].name).signalling
                 self.config.tree.channels[i].winktime = globals.session.query(DahdiSpan).get(self.config.tree.channels[i].name).winktime
             except:
                 print ('error in spans loading')
             
             
             if self.config.tree.channels[i].active == 'yes':
                 self.config.tree.channels[i].enabled = True
             else:
                 self.config.tree.channels[i].enabled = False
             
        	 
        	
        	 
        for line in self.dahdiconfig.tree.lines:
            if line.value.find(':') > -1 :
                line.bits = line.value.split(':')[1]
                line.channel = line.value.split(':')[0]
                #sp = globals.session.query(DahdiChannel).get(line.channel)
                try:
                    line.didnum = sp.did
                    
                except: 
                    print ('channel does not exist in db')
            else :
                line.channel = line.value
            print ('channel is '+line.channel)    
            try:
                globals.session.query(DahdiChannel).get(line.channel).active = True
            except:
                print ('channel does not exist in db')
                
        #globals.session.commit()
            
        
        self.binder.reset(self.config.tree).autodiscover().populate()
        self.binder_lines.reset(self.dahdiconfig.tree).autodiscover().populate()
        #self.binder.update()
        #self.binder.autodiscover().populate()
        #self.binder.reset().autodiscover().populate()
    
    def getDahdiSettings(self):
        p = subprocess.Popen(['dahdi_scan',''], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = p.communicate()
        lines = output.splitlines()
        config = ConfigObj(lines)
        return config
    
    @on('save', 'click')
    def saveSettings(self):
        try:
            self.binder.update()
            self.binder_lines.update()
        except:
            print ('problem in binding for dahdi now return')
            return
        
        userconfig = ''
        for i in range(0, len(self.config.tree.channels)):
            dbspan = globals.session.query(DahdiSpan).get(self.config.tree.channels[i].name)
            dbspan.timing = self.config.tree.channels[i].syncsrc
            dbspan.lbo = self.config.tree.channels[i].lbo
            dbspan.framing = self.config.tree.channels[i].framing
            dbspan.coding = self.config.tree.channels[i].coding
            dbspan.crc = self.config.tree.channels[i].crcenabled
            dbspan.active = self.config.tree.channels[i].enabled
            if  self.config.tree.channels[i].winktime != None: dbspan.winktime = self.config.tree.channels[i].winktime
            dbspan.signalling = self.config.tree.channels[i].signalling

            if dbspan.active == True:
                userconfig = userconfig + '[dahdi_'+str(dbspan.id) +']\n' + 'group = 1\n' +'channel = '+ dbspan.range + '\nsignalling = '+ dbspan.signalling+ '\ncontext=casgw\ndahdichan = '+dbspan.range + '\nmrd_wink_ms = ' + str(dbspan.winktime) + '\n\n\n\n'
            else:
                self.dahdiconfig.tree.spans.remove(self.config.tree.channels[i].spandata)
                continue
            
            if (len(self.config.tree.channels) == len(self.dahdiconfig.tree.spans)):
                if self.config.tree.channels[i].crcenabled == True:
                    self.dahdiconfig.tree.spans[i].value = self.config.tree.channels[i].name + ','+  self.config.tree.channels[i].syncsrc+','+ self.config.tree.channels[i].lbo +','+ self.config.tree.channels[i].framing +','+ self.config.tree.channels[i].coding +',crc4'
                else:
                    self.dahdiconfig.tree.spans[i].value = self.config.tree.channels[i].name + ','+  self.config.tree.channels[i].syncsrc+','+ self.config.tree.channels[i].lbo +','+ self.config.tree.channels[i].framing +','+ self.config.tree.channels[i].coding 
        
        if (len(self.config.tree.channels) != len(self.dahdiconfig.tree.spans)):
            for u in self.dahdiconfig.tree.spans: self.dahdiconfig.tree.spans.remove(u)
            for i in range(0, len(self.config.tree.channels)):
                if self.config.tree.channels[i].enabled == True:
                    
                    newspan = SpanData(node=Node('line', children=[
                         Node('token', children=[PropertyNode('value', 'span')]),
                         Node('token', children=[PropertyNode('value', 'value')]),
                         ]))
                    newspan.span = 'span'
                    if self.config.tree.channels[i].crcenabled == True:
                        newspan.value = self.config.tree.channels[i].name + ','+  self.config.tree.channels[i].syncsrc+','+ self.config.tree.channels[i].lbo +','+ self.config.tree.channels[i].framing +','+ self.config.tree.channels[i].coding +',crc4'
                    else:
                        newspan.value = self.config.tree.channels[i].name + ','+  self.config.tree.channels[i].syncsrc+','+ self.config.tree.channels[i].lbo +','+ self.config.tree.channels[i].framing +','+ self.config.tree.channels[i].coding 
                    self.dahdiconfig.tree.spans.append(newspan)
        
                    
                    
                
            
        print ('span length is now '+str(len(self.dahdiconfig.tree.spans))) 
        
        for u in self.dahdiconfig.tree.lines:
            print(u._node.children[0]['value'].value)
        
        for u in self.dahdiconfig.tree.spans:
            print(u._node.children[1]['value'].value)

    
        fo = open('/etc/asterisk/users_additional.conf','w+')
        fo.writelines(userconfig)
        fo.close()  
        
        for line in self.dahdiconfig.tree.lines:
            if line.protocol == "cas":
                line.value = line.channel + ':'+ line.bits
            else:
                line.value = line.channel
            try:
                globals.session.query(DahdiChannel).get(line.channel).did = line.didnum
                globals.session.query(DahdiChannel).get(line.channel).active = True
            
            except:
                print('channel:' + line.channel +  ' does not have DahdiChannel db param')
                
            globals.session.commit()
                
        self.dahdiconfig.save()
        self.refresh()
        self.context.launch('terminal',command='sh /usr/local/bin/reloadDahdi.sh')
        

    

